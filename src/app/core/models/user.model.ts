export class User
{
  id: number;
  username: string;
  email: string;
  token: string;
  enabled: boolean;

  password: string;
  plainPassword: string;
}
